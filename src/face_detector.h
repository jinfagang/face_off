//
// Created by jintian on 17-12-18.
//

#ifndef FACE_OFF_FACEDETECTOR_H
#define FACE_OFF_FACEDETECTOR_H

/**
 * this class using for only detect face
 * get the bounding boxes.
 */

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <vector>
#include <string>

#include "dlib/image_processing/frontal_face_detector.h"
#include "dlib/image_processing/render_face_detections.h"
#include "dlib/image_processing.h"
#include "dlib/gui_widgets.h"
#include "dlib/image_io.h"
#include "dlib/timer.h"

using namespace std;

struct Detection{
    int x1;
    int y1;
    int x2;
    int y2;
};



/**
 * how to write this class a singleton?????
 * the shape detector just load only once!!!
 */
class FaceDetector {
private:
    // currently we only can write this as static
    string landmarks_f = "/media/jintian/Netac/CodeSpace/ng/ai/lab/vision/face_off"
            "/data/dlib/shape_predictor_68_face_landmarks.dat";

    dlib::frontal_face_detector detector;
    dlib::shape_predictor sp;
    dlib::image_window win;
    dlib::image_window win_faces;

    FaceDetector();

    // private duplicate and sign
    FaceDetector(const FaceDetector&);

    static FaceDetector *instance;

public:
    // FaceDetector is a singleton
    static FaceDetector* getInstance();

    // all the static method are can be access directly and public
    void set_shape_detector_landmarks(string landmarks_f);
    void detect_on_image_file(string image_f);

};


#endif //FACE_OFF_FACEDETECTOR_H
