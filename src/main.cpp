/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * Author: Jin Fagang, All Rights Reserved.
 *
 */


#include <iostream>
#include <vector>
#include <string>
#include "dlib/image_processing/frontal_face_detector.h"
#include "dlib/image_processing.h"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "face_detector.h"

#include "cao/colors.h"
#include "cao/os.h"

using namespace std;
using namespace cao;



/// a simple project
/// \return
int main(int argc, char** argv) {
    std::cout << colors::green << "Hello, face off!" << colors::reset << std::endl;

    FaceDetector* faceDetector = FaceDetector::getInstance();
    faceDetector->detect_on_image_file(argv[1]);

    cout << "Let do it again:\n";
    faceDetector->detect_on_image_file(argv[1]);


    return 0;
}